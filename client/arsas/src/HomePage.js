import React, { useState } from "react";
import ytBir from "./image/YatakOdası/1.jpg";
import ytIki from "./image/YatakOdası/2.jpg";
import ytUc from "./image/YatakOdası/3.jpg";
import { useRouteMatch, BrowserRouter as Router } from "react-router-dom";
import HomePageRouteMenu from "./HomePageRouteMenu";
import "./styles/HomePageRouteMenu.css";

import Slideble from "./Swiper";
import { Col, Button } from "react-bootstrap";
import "./styles/HomePage.css";

export default function HomePage() {
  const { url, path } = useRouteMatch();

  const [isClick, setIsClick] = useState({
    one: true,
    two: false,
    tree: false,
  });
  const divStyle = {
    color: "#9b5a05",
  };

  console.log("========>", isClick);

  return (
    <div className="just">
      <Slideble></Slideble>

      <div className="dropdown-group">
        <div id="features" className="nav-link-button">
          <Button
            variant="outline-primary"
            onClick={() => setIsClick({ one: true, two: false, tree: false })}
          >
            yatak odası
          </Button>

          <Button
            variant="outline-primary"
            onClick={() => setIsClick({ one: false, two: true, tree: false })}
          >
            yatak odası
          </Button>
          <Button
            variant="outline-primary"
            onClick={() => setIsClick({ one: false, two: false, tree: true })}
          >
            yatak odası
          </Button>
        </div>
        {isClick.one ? (
          <HomePageRouteMenu
            imgone={ytBir}
            imgtwo={ytIki}
            imgtree={ytUc}
          ></HomePageRouteMenu>
        ) : isClick.two ? (
          <HomePageRouteMenu
            imgone={ytIki}
            imgtwo={ytUc}
            imgtree={ytBir}
          ></HomePageRouteMenu>
        ) : (
          <HomePageRouteMenu
            imgone={ytBir}
            imgtwo={ytIki}
            imgtree={ytUc}
          ></HomePageRouteMenu>
        )}
      </div>
    </div>
  );
}

/*
   /MENU UL
 
 <ul className="menu-ul">
                  <div className="den">
                    {" "}
                    <a href="/">yemk odası takımları</a>
                    <br></br>
                    <a href="/">ModerN Yemek odası takımları </a>
                    <br></br>
                    <a href="/">ModerN Yemek odası takımları </a>
                  </div>
                </ul>



*/
