import React from "react";
import { Carousel, Container, Image } from "react-bootstrap";
export default function Slideble() {
  return (
    <div>
      <Container>
        <Carousel>
          <Carousel.Item interval={2000}>
            <img
              className="d-block w-100 "
              src="https://img.lovepik.com/photo/40054/2131.jpg_wh860.jpg"
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item interval={2000}>
            <img
              className="d-block w-100"
              src="https://img.lovepik.com/photo/40054/2131.jpg_wh860.jpg"
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item interval={2000}>
            <img
              className="d-block w-100"
              src="https://img.lovepik.com/photo/40054/2131.jpg_wh860.jpg"
              alt="First slide"
            />
          </Carousel.Item>
        </Carousel>
      </Container>
    </div>
  );
}
