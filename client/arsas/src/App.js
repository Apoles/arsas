import React from "react";
import HomePage from "./HomePage";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MainPages from "./MainPages";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <MainPages chl={<HomePage></HomePage>}></MainPages>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
